﻿namespace GibixOnline.ReaderShareProject {
	#region Using directives

	using System;
	using System.Web;
	using System.Web.Mvc;
	using System.Web.Routing;

	#endregion

	/// <summary>
	/// Main setup point for the MVC application.
	/// </summary>
	public class MvcApplication : HttpApplication {
		/// <summary>
		/// Registers the global filters.
		/// </summary>
		/// <param name="filters">The filters.</param>
		static void RegisterGlobalFilters(GlobalFilterCollection filters) { filters.Add(new HandleErrorAttribute()); }

		/// <summary>
		/// Registers the routes.
		/// </summary>
		/// <param name="routes">The routes.</param>
		static void RegisterRoutes(RouteCollection routes) {
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			routes.MapRouteLowercase("static-index", String.Empty, new { controller = "Static", action = "Index" });
			routes.MapRouteLowercase("feed", "feed/{token}/{action}.{format}", new { controller = "Feed" }, new { token = @"[a-f0-9]{32}" });
			routes.MapRouteLowercase("share", "post/{token}/share", new { controller = "Post", action = "Share" }, new { token = @"[a-f0-9]{32}" });
			routes.MapRouteLowercase("controller-id", "{controller}/{action}/{id}.{format}", new { controller = "Static", action = "Index" }, new { id = @"\d+" });
			routes.MapRouteLowercase("controller-action-format", "{controller}/{action}.{format}", new { controller = "Static", action = "Index" });
			routes.MapRouteLowercase("controller-action", "{controller}/{action}", new { controller = "Static", action = "Index" });
		}

		/// <summary>
		/// Called on application start.
		/// </summary>
		protected void Application_Start() {
			AreaRegistration.RegisterAllAreas();

			RegisterGlobalFilters(GlobalFilters.Filters);
			RegisterRoutes(RouteTable.Routes);
		}
	}
}
