﻿namespace GibixOnline.ReaderShareProject.Controllers {
	#region Using directives

	using System;
	using System.Collections.Generic;
	using System.Data;
	using System.Linq;
	using System.ServiceModel.Syndication;
	using System.Web.Mvc;

	using Dapper;

	using GibixOnline.ReaderShareProject.Models;
	using GibixOnline.ReaderShareProject.Support;

	#endregion

	public class FeedController : BaseController {
		[HttpGet]
		public ActionResult Shares() {
			string formatType = this.RouteData.Values["format"] as string;
			Guid token;
			if (String.IsNullOrWhiteSpace(formatType) || !Guid.TryParse(this.RouteData.Values["token"] as string, out token)) {
				return this.HttpNotFound();
			}

			List<Post> posts = new List<Post>();
			User user;

			using (IDbConnection connection = Engine.GetConnection()) {
				connection.Open();

				var queries = connection.QueryMultiple(
					"SELECT Published, LastUpdated, Title, Summary, OriginalUri OriginalUriString, Content FROM reader.user_feed_view WHERE Token = :Token AND Public = true AND LastUpdated >= :LastUpdated; SELECT EmailMD5, Name, Email FROM reader.users WHERE token = :Token;",
					new {
						Token = token,
						LastUpdated = DateTime.Now.AddMonths(-1)
					});

				posts.AddRange(queries.Read<Post>());
				user = queries.Read<User>().SingleOrDefault();

				connection.Close();
			}

			if (user == null || posts.Count == 0) {
				return this.HttpNotFound();
			}

			List<SyndicationItem> items = new List<SyndicationItem>();
			foreach (Post post in posts.OrderByDescending(p => p.LastUpdated)) {
				SyndicationItem item = new SyndicationItem();
				if (post.OriginalUri != null) {
					item.AddPermalink(post.OriginalUri);
				}

				item.Title = new TextSyndicationContent(post.Title, TextSyndicationContentKind.Plaintext);
				if (!String.IsNullOrWhiteSpace(post.Summary)) {
					Uri uri;
					if (Uri.TryCreate(post.Summary, UriKind.Absolute, out uri)) {
						item.AddPermalink(uri);
					} else {
						item.Content = new TextSyndicationContent(post.Summary, TextSyndicationContentKind.Plaintext);
					}
				}

				if (!String.IsNullOrWhiteSpace(post.Content)) {
					item.Content = new TextSyndicationContent(post.Content, TextSyndicationContentKind.Html);
				}

				items.Add(item);
			}

			SyndicationFeed feed = new SyndicationFeed {
				Copyright = new TextSyndicationContent("All content copyright of their respective owners. This site claims no copyright over the shared content."),
				Description = new TextSyndicationContent("Contains the last month of {0}'s shared items".FormatIC(user.Name), TextSyndicationContentKind.Plaintext),
				Generator = "ReaderShareProject/1.0",
				ImageUrl = new Uri("https://secure.gravatar.com/avatar/{0:n}?s=128".FormatIC(user.EmailMD5)),
				Items = items,
				Language = "en-US",
				LastUpdatedTime = posts.Max(p => p.LastUpdated),
				Title = new TextSyndicationContent("{0}'s shared items".FormatIC(user.Name), TextSyndicationContentKind.Plaintext)
			};
			feed.Authors.Add(new SyndicationPerson(user.Email, user.Name, null));

			if (String.CompareOrdinal(formatType, "atom") == 0) {
				return new AtomActionResult(feed);
			}

			if (String.CompareOrdinal(formatType, "rss") == 0) {
				return new RssActionResult(feed);
			}

			return this.RedirectToAction("Index", "Static");
		}
	}
}
