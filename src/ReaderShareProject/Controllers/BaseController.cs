﻿namespace GibixOnline.ReaderShareProject.Controllers {
	#region Using directives

	using System.Data;
	using System.Linq;
	using System.Web.Mvc;

	using Dapper;

	using GibixOnline.ReaderShareProject.Models;
	using GibixOnline.ReaderShareProject.Support;

	#endregion

	/// <summary>
	/// Contains the base properties for the controllers in the application.
	/// </summary>
	public class BaseController : Controller {
		/// <summary>
		/// Contains the cached reader user object during the current call.
		/// </summary>
		User readerUser;

		/// <summary>
		/// Gets or sets the reader user.
		/// </summary>
		protected User ReaderUser {
			get {
				if (this.User.Identity.IsAuthenticated && this.readerUser == null) {
					using (IDbConnection connection = Engine.GetConnection()) {
						connection.Open();
						this.ReaderUser =
							connection.Query<User>(
								"SELECT Id, Created, Email, EmailMD5, LastLoggedIn, Name, Token, PostingKey FROM reader.user_identifiers WHERE Identifier = :Identifier",
								new { Identifier = this.User.Identity.Name }).SingleOrDefault();
						connection.Close();
					}
				}

				return this.readerUser;
			}

			set { this.readerUser = value; }
		}
	}
}
