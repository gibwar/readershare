﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GibixOnline.ReaderShareProject.Support {
	using System.Security.Cryptography;
	using System.Text;

	internal static class Utilities {
		internal static Guid MD5Hash(string input) {
			byte[] hash;
			using (MD5 md5 = MD5.Create()) {
				hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
			}
			StringBuilder result = new StringBuilder(hash.Length);
			foreach (byte b in hash) {
				result.AppendFormat("{0:x2}", b);
			}
			return Guid.Parse(result.ToString());
		}
	}
}